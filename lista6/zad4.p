set terminal pdf
set output "siatka.pdf"
set grid
set style data linesp
set key autotitle columnhead
plot "data.txt" u 1:2, "" u 1:3, "" u 1:4



set terminal pdf
set output "siatka1.pdf"
set grid polar
set style data linesp
set key autotitle columnhead
plot "data.txt" u 1:2, "" u 1:3, "" u 1:4

set terminal pdf
set output "siatka2.pdf"
set grid linewidth 8
set style data linesp
set key autotitle columnhead
plot "data.txt" u 1:2, "" u 1:3, "" u 1:4

set terminal pdf
set output "siatka3.pdf"
set grid linestyle 12
set style data linesp
set key autotitle columnhead
plot "data.txt" u 1:2, "" u 1:3, "" u 1:4


set terminal pdf
set output "siatka4.pdf"
set grid layerdefault
set style data linesp
set key autotitle columnhead
plot "data.txt" u 1:2, "" u 1:3, "" u 1:4



